class Service {
    setManagementMenu = (pageId: string) => { 
        // console.log('pageId', pageId)
        return [
            // {
            //     type: 'header',
            //     title: 'management'
            // },
            {
                type: 'item',
                title: 'dashboard',
                link: 'dashboard',
                icon: 'dashboard'
            },
            {
                type: 'item',
                title: 'payments',
                // link: '',
                icon: 'payment'
            },
            {
                type: 'item',
                title: 'invitations',
                // link: '',
                icon: 'invitations'
            },
            {
                type: 'item',
                title: 'rsvp',
                link: 'rsvp',
                icon: 'approvals'
            },
            {
                type: 'item',
                title: 'surveys',
                // link: `/${pageId}/survey`,
                icon: 'survey'
            },
            {
                type: 'item',
                title: 'giftcards',
                link: 'giftcard',
                icon: 'gift-cards'
            },
            {
                type: 'item',
                title: 'promotions',
                // link: '',
                icon: 'promotions'
            },
            // {
            //     type: 'header',
            //     title: 'system'
            // },
            {
                type: 'item',
                title: 'employees',
                // link: `/${pageId}/employees`,
                icon: 'profile'
            },
            // {
            //     type: 'item',
            //     hideDesktop: true,
            //     title: 'switchVenue',
            //     // link: '/venues',
            //     icon: 'payment'
            // },
            // {
            //     type: 'item',
            //     hideDesktop: true,
            //     title: 'logout',
            //     // link: '/logout',
            //     icon: 'logout'
            // }
        ]
    };
}

export default new Service()
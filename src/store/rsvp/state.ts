export interface state {
  filterKey: string;
  dateKey: any;
  filterDates: boolean;
  rsvpList: Array<any> ;
}

function state(): state {
  return {
    filterKey: '',
    filterDates: false,
    dateKey: null,
    rsvpList: [],
  }
};

export default state;

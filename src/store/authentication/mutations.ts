import { MutationTree } from 'vuex';
import { state } from './state';

const mutation: MutationTree<state> = {
  set (state , { name, val }) {
    state[name] = val
  },
  setAuth (state , payload: boolean) {
    state.auth = payload
  },
  setVenues (state , payload: any) {
    state.venues = payload
  },
}

export default mutation;

export interface state {
  venues: any;
}

function state(): state {
  return {
    venues: {
      system: [],
      user: [],
      search: []
    }
  }
};

export default state;

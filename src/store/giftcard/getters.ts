import { GetterTree } from 'vuex';
import { StateInterface } from '../index';
import { state } from './state';

const getters: GetterTree<state, StateInterface> = {
    getGiftcards(state) {
    const list: any[] = [];
      state.giftcards.forEach((giftcard) => {
        const selectedType = state.giftcardTypes.find((type) => type.id === giftcard.type) || {}
        const item: any = {
          ...giftcard,
          terms: selectedType.terms,
          label: selectedType.label
        };
        list.push(item)
      })
      return list
    },
    getGiftcard(state, getters) {
        const id = state.giftcardId;
        const giftcards = getters['getGiftcards']
        return giftcards.find((giftcard => giftcard.id === id))
    }
};

export default getters;
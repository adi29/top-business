import { GetterTree } from 'vuex';
import { StateInterface } from '../index';
import { state } from './state';
import moment from 'moment'

const getters: GetterTree<state, StateInterface> = {
  rsvpSummary (state, getters) {
    const list: Array<any> = getters['filterRsvp'] || [];
    const total: number = list.length;
    return list.reduce((acc, item) => {
      if (item.status === 'canceled')  {
        acc.canceled++
      }
      if (item.answer === 'approved')  {
        acc.approved++
      }
      if (item.status === 'invited')  {
        acc.send++
      }
      if (item.viewedAt !== 'Invalid Date')  {
        acc.watch++
      }
      if (item.answer)  {
        acc.replied++
      }
      if (item.continue)  {
        acc.procces++
      }
      return acc
    }, { total, canceled: 0, approved: 0, send: 0, watch: 0, replied: 0, procces: 0 })
  },

  filterRsvp (state, getters) {
    const list: Array<any> = getters['rsvpList'];
    if (!state.filterDates) {
      return list;
    }
    const dateKey: any = state.dateKey;
    let from = dateKey.from;
    from = moment(from, ['DD/MM/YY'])
    let to = dateKey.to || dateKey.from  //   case of range || case of single day
    to = moment(to, ['DD/MM/YY']);
    return list.filter(item => {
      let date = item.expected.slice(0,8);
      date = moment(date, ['YYYYMMDD']);
      if (date >= from && date <= to) return item
    })
  },

  rsvpList (state) {
    const key:string = state.filterKey;
    const list: Array<any> = state.rsvpList || [];
    if (!key) {
      return list;
    }
    return list.filter((item) => {
      switch (key) {
        case 'total':
          return true                  
        case 'canceled':          
            return item.status === 'canceled'                  
        case 'approved':          
            return item.answer === 'approved'                  
        case 'send':          
          return item.status === 'invited'        
        case 'watch':          
          return item.viewedAt !== 'Invalid Date'        
        case 'replied':          
          return item.answer        
        case 'procces':          
          return item.continue        
      }

      return false
    })
  },
 
};

export default getters;
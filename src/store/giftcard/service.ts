import { ontopoIo, gql } from 'src/boot/graphql-http-client';
import { i18n } from 'src/boot/i18n'

class Service {
  public getGiftcards = () => {
    return [
      {id: '123459', name: 'לוק סקייווקר', phone: '0549876543', type: 'businessLunch', total_value: 300, balance: 300, expirity: 365, util: [] },
      {id: '123457', name: 'פרינסס ליה', phone: '0549876543', type: 'businessLunch', total_value: 300, balance: 300, expirity: 365, util: [] },
      {id: '123456', name: 'דארת׳ ווידר', phone: '0549876543', type: 'brunch', total_value: 250, balance: 250, expirity: 365, util: [] },
      {id: '123458', name: 'אובי ואן קנובי', phone: '0549876543', type: 'businessLunch', total_value: 200, balance: 0, expirity: 365, util: [{name: 'ג׳ורג׳ לוקאס', value: '200', date: '20210413'}] },
      {id: '123460', name: 'מאסטר יודה', phone: '0549876543', type: 'weekend', total_value: 300, balance: 300, expirity: 365, util: [] },
    ]
  };

  public getGiftcardsTypes = () => {
    return [
      { id: 'brunch', label :'בראנץ', terms: ['תקף באמצע השבוע בלבד.', 'לא תקף בעסקיות']},
      { id: 'businessLunch', label :'עיסקית צהריים', terms: ['תקף באמצע השבוע בלבד.']},
      { id: 'weekend', label :'ארוחת צהריים/ ערב', terms: ['תקף גם לסופי שבוע']}
    ]
  }

  public getManagers = () => {
    return ['ג׳ורג׳ לוקאס', 'קארי פישר', 'הריסון פורד']
  }
}

export default new Service();

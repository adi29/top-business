import { store } from 'quasar/wrappers';
import { createStore } from 'vuex';

import application from './application'
import authentication from './authentication'
import rsvp from './rsvp'
import giftcard from './giftcard'
import user from './user'
import venues from './venues'
// import { ExampleStateInterface } from './module-example/state';

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Store instance.
 */

export interface StateInterface {
  // Define your own store structure, using submodules if needed
  // example: ExampleStateInterface;
  // Declared as unknown to avoid linting issue. Best to strongly type as per the line above.
  application: unknown;
  authentication: unknown;
  rsvp: unknown;
  giftcard: unknown;
  user: unknown;
  venues: unknown;
}

export default store(function (/* { ssrContext } */) {
  const Store = createStore<StateInterface>({
    modules: {
      application,
      authentication,
      rsvp,
      giftcard,
      user,
      venues
    },

    // enable strict mode (adds overhead!)
    // for dev mode and --debug builds only
    strict: !!process.env.DEBUGGING,
    devtools: true
  });

  return Store;
});

import { ontopoIo, gql } from 'src/boot/graphql-http-client';
import { i18n } from 'src/boot/i18n'

class Service {
  // Service: get single venue details (not same as venuesDetails)
  public singleVenueDetails = async venue => {
    const venueQuery = gql`
      query venueDetails($input: VenuesDetailsInput!) {
        venueDetails(input: $input)
      }
    `;

    try {
      const venueDetails = await ontopoIo.query('venueDetails', {
        query: venueQuery,
        variables: {
          input: {
            venues: [venue],
            locale: i18n.global.locale
          }
        }
      });

      return venueDetails[0];
    } catch (error) {
      throw {
        statusCode: 400,
        error: JSON.stringify(error)
      };
    }
  };

  // Service: Get user venues
  public userVenues = async () => {
    const queryGql = gql`
      query UserVenues {
        userVenues {
          venues
        }
      }
    `;

    const data = await ontopoIo.query('userVenues', {
      query: queryGql,
      variables: null
    });

    if (data.venues) {
      try {
        const venueQuery = gql`
          query venuesDetails($input: VenuesDetailsInput!) {
            venuesDetails(input: $input)
          }
        `;

        const venues = await ontopoIo.query('venuesDetails', {
          query: venueQuery,
          variables: {
            input: {
              venues: data.venues,
              locale: i18n.global.locale
            }
          }
        });
        return venues;
      } catch (error) {
        console.log(error);
        return error;
      }
    } else {
      throw {
        statusCode: 400,
        error: JSON.stringify(data),
        message: data.message?.replace('GraphQL error: ', '')
      };
    }
  };

  // query for autocomplete venuesearch
  public autocompleteIndex = async ({ directory, terms }: autocompleteIndexInput) => {
    const queryGql = gql`
      query autocompleteIndex($input: AutocompleteIndexFilterInput!) {
        autocompleteIndex(input: $input) {
          found
          total
          suggestions {
            cmsId
            label
            subtitle
          }
        }
      }
    `;

    const locale = i18n.global.locale == 'en-us' ? 'en' : i18n.global.locale;

    const variables = {
      input: {
        directory,
        locale,
        terms
      }
    };

    const data = await ontopoIo.query('autocompleteIndex', {
      query: queryGql,
      variables
    });
    return data;
  };
}

interface autocompleteIndexInput {
  directory: string;
  terms: string;
}

export default new Service();

import { ActionTree } from 'vuex';
import { StateInterface } from '../index';
import { state } from './state';
import service from './service';
import moment from 'moment';

const actions: ActionTree<state, StateInterface> = {
  getGiftcards({ commit }) {
    const giftcards = service.getGiftcards();
    commit('set', { name: 'giftcards', val: giftcards });
    
    const giftcardTypes = service.getGiftcardsTypes();
    commit('set', { name: 'giftcardTypes', val: giftcardTypes });
    
    const managers = service.getManagers();
    commit('set', { name: 'managers', val: managers });
  },

  addNewGiftcard({state, commit}, data) {
    //  temp assign
    const payload = {
      ...data,
      id: +state.giftcards[state.giftcards.length -1].id + 1,
      total_value: data.value,
      balance: data.value,
      util: []
    }
    commit('addGiftcard', payload)
  },

  editGiftcard({state, commit}, data) {
    //  temp assign
    const util = {
      name: data.name, 
      value: data.value, 
      date: moment().locale('he').format('DD/MM/YY')
    }
    const id = state.giftcardId;
    const idx = state.giftcards.findIndex((giftcard => giftcard.id === id))
    commit('addUtil', { util, idx })
  }
};

export default actions;

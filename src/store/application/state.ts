export interface state {
  locale: string;
  prop: boolean;
  cmsId: string;
  activeLink: string;
  displayKey: string;
  menuList: Array<any> ;
}

function state(): state {
  return {
    locale: 'he',
    prop: false,
    cmsId: '123456',
    activeLink: '',
    displayKey: 'list',
    menuList: [],
  }
};

export default state;

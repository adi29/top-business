import Vue from 'vue';
import { MutationTree } from 'vuex';
import { state } from './state';

const mutation: MutationTree<state> = {
  setRsvp (state , payload: Array<any>) {
    state.rsvpList = payload;
  },
  setFilterKey (state , payload: string) {
    state.filterKey = payload;
  },
  setDateKey (state , payload: any) {
    state.dateKey = payload;
  },
  setFilterDates (state , payload: boolean) {
    state.filterDates = payload;
  }
}

export default mutation;

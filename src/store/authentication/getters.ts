import { GetterTree } from 'vuex';
import { StateInterface } from '../index';
import { state } from './state';

const getters: GetterTree<state, StateInterface> = {
  isAuth (state) {
    return state.auth
  },
};

export default getters;
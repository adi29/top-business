import Vue from 'vue';
import { MutationTree } from 'vuex';
import { state } from './state';

// export const set = (state: any, { name, val }) => {
//   if (state.hasOwnProperty(name)) {
//     // state[name] = val;
//     Vue.set(state, name, val); // in case of !state[name]
//   }
// };
const mutation: MutationTree<state> = {
  setLocale (state , payload: string) {
    state.locale = payload
  },
  setCmsId (state , payload: string) {
    state.cmsId = payload
  },
  setMenu (state , payload: Array<string>) {
    state.menuList = payload;
  },
  setActiveLink (state , payload: string) {
    state.activeLink = payload;
  },
  setDisplay (state , payload: string) {
    state.displayKey = payload;
  },
}

export default mutation;

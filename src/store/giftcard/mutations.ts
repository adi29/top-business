import { MutationTree } from 'vuex';
import { state } from './state';

const mutation: MutationTree<state> = {
  set(state: any, { name, val }) {
    if (state.hasOwnProperty(name)) {
      state[name] = val;
    }
  },

  addGiftcard(state: any, payload) {
    state.giftcards.push(payload)
  },

  addUtil(state: any, payload) {
    const { util, idx } = payload;
    state.giftcards[idx].util.push(util)
    state.giftcards[idx].balance = state.giftcards[idx].balance - util.value;
  }
};

export default mutation;

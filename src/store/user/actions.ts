import { ActionTree } from 'vuex';
import { StateInterface } from '../index';
import { state } from './state';

import profileService from './service';

const actions: ActionTree<state, StateInterface> = {
  async getProfile({ commit }) {
    const currentUser = await profileService.userProfile();
    commit('setLoginUser', {
      authenticated: true,
      ...currentUser,
    });
  }
};

export default actions;

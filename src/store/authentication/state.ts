export interface state {
  error: string | null;
  auth: boolean;
  venues: Array<any>
}

function state(): state {
  return {
    error: null,
    auth: false,
    venues: []
  }
};

export default state;

import { SubscriptionClient } from 'subscriptions-transport-ws';
import { ontopoIo } from 'src/boot/graphql-http-client';

class WebSocketService {
  uri = 'wss://dbio4.ontopo.work/v1/graphql';
  subscription: any = {};
  wsClient: any = {};

  public stopAll() {
    const subscription = JSON.parse(JSON.stringify(this.subscription));
    Object.keys(subscription).forEach((key: string) => {
      this.stop(key);
    });
  }

  public stop(id: any) {
    if (this.subscription && this.subscription[id]) {
      this.subscription[id].unsubscribe();
      this.subscription[id] = null;
    }

    if (this.wsClient && this.wsClient[id]) {
      this.wsClient[id].close(true, true);
      this.wsClient[id] = null;
    }
  }

  public checkSubscribe(id: any) {
    return (
      this.wsClient &&
      this.wsClient[id] &&
      this.subscription &&
      this.subscription[id]
    );
  }

  public async subscribe(
    name: any,
    { id, query, variables }: SubscribeInput,
    callback: any
  ) {
    this.stop(id);
    const token = await ontopoIo.getToken();

    if (!this.wsClient[id]) {
      Object.assign(this.wsClient, { [id]: null });
    }
    this.wsClient[id] = new SubscriptionClient(this.uri, {
      reconnect: true,
      connectionParams: {
        headers: {
          Authorization: `Bearer ${token || ''}`
        }
      },
      timeout: 1000 * 60,
      connectionCallback: () => {
        console.log('SubscriptionClient.connectionCallback:');
      },
      reconnectionAttempts: 10000,
      inactivityTimeout: 10 * 1000,
      lazy: true
    });

    const ret = this.wsClient[id].request({ query, variables });

    if (!this.subscription[id]) {
      Object.assign(this.subscription, { [id]: null });
    }
    this.subscription[id] = ret.subscribe({
      next: ({ data }: any) => {
        if (process.env.DEV) {
          console.log('subscription emitted => trigger run');
          console.dir(data);
        }
        if (name) {
          callback(data[name], id);
        } else {
          callback(data, id);
        }
      },
      error: (error: any) => {
        if (error.message.indexOf('JWTExpired') > 0) {
          if (process.env.DEV) {
            console.log('Websocket JWT expired');
          }

          localStorage.removeItem('auth_token_expired');
          this.subscribe(name, { id, query, variables }, callback).catch(err =>
            console.error(err)
          );
        } else {
          if (process.env.DEV) {
            console.log('Websocket got error:');
            console.dir(error);
          }
        }
      }
    });
  }
}

export default new WebSocketService();

interface SubscribeInput {
  id: any;
  query: string;
  variables?: any;
}

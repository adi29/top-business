import { MutationTree } from 'vuex';
import { state } from './state';

const mutation: MutationTree<state> = {
  set(state: any, { name, val }) {
    if (state.hasOwnProperty(name)) {
      state[name] = val;
    }
  },

  setVenues(state: any, { name, venues }) {
    state.venues[name] = venues
  }
};

export default mutation;

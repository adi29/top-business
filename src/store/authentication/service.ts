import moment from 'moment';
import { ontopoIo, gql } from 'src/boot/graphql-http-client';
import { i18n } from 'src/boot/i18n';

class Service {
  setVenues = (userId: string) => {
    console.log('pageId', userId);
    return ['12346'];
  };

  // Service: Sign up + send sms code
  public signupSendCode = async ({
    firstName,
    lastName,
    email,
    phone,
    regionCode,
    recaptcha,
  }: SignupSendCodeInput) => {
    if ((!phone || !regionCode) && !email) {
      throw new Error('Invalid signup input');
    }

    localStorage.removeItem('auth_token');
    localStorage.removeItem('auth_refresh_token');
    sessionStorage.removeItem('venue_id');

    const mutationGql = gql`
      mutation SignupSendCode($input: SignupInput!) {
        signupSendCode(input: $input) {
          statusCode
          error
          message
        }
      }
    `;

    const res = await ontopoIo.mutate('signupSendCode', {
      mutation: mutationGql,
      variables: {
        input: {
          firstName,
          lastName,
          email,
          phone,
          regionCode,
          recaptcha,
        },
      },
    });

    return {
      data: !res['error'] && res,
      error: res['error']
    };
  };

  // Service: Login + send sms code
  public loginSendCode = async ({
    phoneOrEmail,
    regionCode,
    recaptcha,
  }: LoginSendCodeInput) => {
    localStorage.removeItem('auth_token');
    localStorage.removeItem('auth_refresh_token');
    sessionStorage.removeItem('venue_id');

    const mutationGql = gql`
      mutation LoginSendCode($input: LoginVerifyInput!) {
        loginSendCode(input: $input) {
          statusCode
          error
          message
        }
      }
    `;

    const res = await ontopoIo.mutate('loginSendCode', {
      mutation: mutationGql,
      variables: {
        input: {
          phoneOrEmail,
          regionCode,
          recaptcha,
          locale: i18n.global.locale,
        },
      },
    });

    return {
      data: !res['error'] && res,
      error: res['error'],
    };
  };

  // Service: Login
  public login = async ({ phoneOrEmail, regionCode, code }: LoginInput) => {
    const mutationGql = gql`
      mutation Login($input: LoginInput!) {
        login(input: $input) {
          jwt_token
          refresh_token
        }
      }
    `;

    const res = await ontopoIo.mutate('login', {
      mutation: mutationGql,
      variables: {
        input: {
          phoneOrEmail,
          regionCode,
          code,
        },
      },
    });

    if (res.jwt_token) {
      const expired = moment().utc().add('10', 'minutes').format();

      localStorage.setItem('auth_token', res.jwt_token);
      localStorage.setItem('auth_refresh_token', res.refresh_token);
      localStorage.setItem('auth_token_expired', expired.toString());

      return {
        statusCode: 200,
      };
    } else {
      return {
        statusCode: 400,
        error: res['error'],
      };
    }
  };
}

export interface SignupSendCodeInput {
  firstName: string;
  lastName: string;
  email?: string;
  phone?: string;
  regionCode?: string;
  recaptcha: string;
}

export interface LoginSendCodeInput {
  phoneOrEmail: string;
  regionCode: string;
  recaptcha: string;
}

export interface LoginInput {
  phoneOrEmail: string;
  regionCode: string;
  code: string;
}

export default new Service();

import { GetterTree } from 'vuex';
import { StateInterface } from '../index';
import { state } from './state';

const getters: GetterTree<state, StateInterface> = {
  menuList (state) {
    const list: Array<any> = state.menuList || [];
    return list && list.filter((item: any) => item && item.type === 'item')
  },
};

export default getters;
import { MutationTree } from 'vuex';
import { state } from './state';

const mutation: MutationTree<state> = {
  set(state: any, { name, val }) {
    if (state.hasOwnProperty(name)) {
      state[name] = val;
    }
  },
  setLoginUser (state, payload) {
    Object.assign(state.profile, { ...state.profile, ...payload })
  }
};

export default mutation;

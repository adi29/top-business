import { ActionTree } from 'vuex';
import { StateInterface } from '../index';
import { state } from './state';

import venuesService from './service';

const actions: ActionTree<state, StateInterface> = {
  async getVenues({ commit, dispatch }) {
    const systemVenues = await dispatch('getSystemVenues');
    const userVenues = [
      {cms_id: 'taizu', title: 'טאיזו', subtitle: 'תל אביב'},
      {cms_id: 'hadson', title: 'הדסון', subtitle: 'חיפה'},
      {cms_id: 'ocd', title: 'ocd', subtitle: 'ירושלים'},
    ]
    // const userVenues = await venuesService.userVenues();
    const venues = {
      system: systemVenues,
      user: userVenues
    }

    commit('set', { name: 'venues', val: venues });
  },

  async getSystemVenues({ rootGetters }) {
    if (!rootGetters['user/isSuperuser']) { return [] }

    const systemVenues: Array<any> = [];
    try {
      const venues = ['ZBryZmnYb', 'g1rTiBEUiP'];

      for (let i = 0; i < venues.length; i++) {
        const venue = venues[i];
        const result = await venuesService.singleVenueDetails(venue);
        if (result) {
          systemVenues.push(result);
        }
      }

      return systemVenues;
    } catch (err) {
      console.error(err)
      return systemVenues;
    }
  },

  async searchVenues ({ state, commit, rootGetters }, { terms }) {
    let venues: Array<any>

    if (rootGetters['user/isSuperuser']) {
      const res = await venuesService.autocompleteIndex({
        directory: 'culinary.venues.il',
        terms
      })
      venues = res.suggestions.map((s: any) => ({ ...s, title: s.label, cms_id: s.cmsId }))
    } else {
      venues = state.venues.user.filter((v: any) => v.title.includes(terms))
    }

    commit('setVenues', { name: 'search', venues })
  }
};

export default actions;

import Vue from 'vue'

export default ({
    computed: {
        isDesktop() {
            return this.$q.screen.gt.sm;
        },
        smallDevice() {
            console.log('small', this.$q.screen)
            return this.$q.screen.width < 360
        }
    }
})
import { ActionTree } from 'vuex';
import { StateInterface } from '../index';
import { state } from './state';
import service from './service'


const actions: ActionTree<state, StateInterface> = {
  setRsvp ({commit}) {
    const rsvpList = service.setRsvp();
    commit('setRsvp', rsvpList)
  }
};

export default actions;

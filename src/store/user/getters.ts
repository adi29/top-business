import { GetterTree } from 'vuex';
import { StateInterface } from '../index';
import { state } from './state';

const getters: GetterTree<state, StateInterface> = {
    isSuperuser (state) {
        return state.profile.is_superuser
    },
};

export default getters;
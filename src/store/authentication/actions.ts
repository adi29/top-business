import { ActionTree } from 'vuex';
import { StateInterface } from '../index';
import { state } from './state';

import authService, {
  SignupSendCodeInput,
  LoginSendCodeInput,
  LoginInput,
} from './service';

const actions: ActionTree<state, StateInterface> = {
  // Signup send code
  async signupSendCode({ commit }, input: SignupSendCodeInput) {
    const { firstName, lastName, email, phone, regionCode } = input;

    // Reset error message
    commit('set', { name: 'error', val: null });

    const res = await authService.signupSendCode({
      firstName,
      lastName,
      email,
      phone,
      regionCode,
      recaptcha: 'ontopo',
    });

    if (res.error) {
      commit('set', { name: 'error', val: 'פרטים לא נכונים' });
      return { status: 'error', error: res.error };
    }

    return { status: 'success', data: res.data };
  },

  // Send verification code to an existing user using phone or email
  async loginSendCode(
    { commit },
    { phoneOrEmail, regionCode }: LoginSendCodeInput
  ) {
    const recaptcha = 'ontopo'; // TEMP

    // Reset error message
    commit('set', { name: 'error', val: null });

    const res = await authService.loginSendCode({
      phoneOrEmail,
      regionCode,
      recaptcha,
    });

    if (res.error) {
      commit('set', { name: 'error', val: 'אימייל או טלפון לא נכונים' });
      return { status: 'error', error: res.error };
    }

    return { status: 'success', data: res.data };
  },

  // User sends the sms code to server for verification and login
  async login({ commit, dispatch }, input: LoginInput) {
    try {
      const { phoneOrEmail, regionCode, code } = input;

      // Reset error message
      commit('set', { name: 'error', val: null });

      const res = await authService.login({
        phoneOrEmail,
        regionCode,
        code,
      });

      if (res.error) {
        commit('set', { name: 'error', val: 'קוד לא נכון' });
        return { status: 'error', error: res.error };
      }

      await dispatch('user/getProfile', {}, { root: true });

      await dispatch('venues/getVenues', {}, { root: true });

      commit('setAuth', true);

      return { status: 'success' };
    } catch (err) {}
  },

  async checkLogin({ state, commit, dispatch }) {
    try {
      // Check if user authenticated
      if (state.auth) {
        return true;
      }

      // Check if has jwt token
      const hasToken = localStorage.getItem('auth_token');
      if (!hasToken) {
        return false;
      }

      // get user data
      await dispatch('user/getProfile', {}, { root: true });

      await dispatch('venues/getVenues', {}, { root: true });

      return true;
    } catch {
      return false;
    }
  },

  clearAuth() {
    localStorage.removeItem('auth_token_expired');
    localStorage.removeItem('auth_token');
    localStorage.removeItem('auth_refresh_token');
    sessionStorage.removeItem('venue_id');
  },

  setVenues({ commit }, userId: string) {
    const venues = authService.setVenues(userId);
    commit('setVenues', venues);
  },
  async connect({ commit, dispatch }, code) {
    commit('setAuth', true);
    await dispatch('venues/getVenues', {}, { root: true });
  },
};

export default actions;

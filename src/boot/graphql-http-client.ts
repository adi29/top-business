import moment from 'moment'
import { GraphQLClient, gql } from 'graphql-request';
export { gql } from 'graphql-request';

class HttpService {
  uri: string;

  constructor(server: string) {
    this.uri = server;
  }

  private async getGraphqlClient(authHeader = true) {
    const graphQLClient = new GraphQLClient(this.uri);

    // Get User Token
    if (authHeader) {
      if (localStorage.getItem('auth_token')) {
        const token = await this.getToken();
        if (token) {
          graphQLClient.setHeader('authorization', `Bearer ${token}`);
        }
      }
    }

    graphQLClient.setHeader('X-Hasura-Client-Name', 'application');

    return graphQLClient;
  }

  private async refreshToken() {
    const token = localStorage.auth_refresh_token;
    if (!token) throw new Error('Invalid auth refresh token');

    localStorage.removeItem('auth_token');

    const mutation = gql`
      mutation RefreshToken($input: RefreshTokenInput!) {
        refreshToken(input: $input) {
          jwt_token
          refresh_token
        }
      }
    `;

    const mutateName = 'refreshToken'
    const input = { refresh_token: token }
    const data = await this.mutate(mutateName, {
      mutation: mutation,
      variables: {
        input,
      },
    });

    if (data.jwt_token) {
      const expired = moment()
        .utc()
        .add('10', 'minutes')
        .format();

      localStorage.setItem('auth_token', data.jwt_token);
      localStorage.setItem('auth_refresh_token', data.refresh_token);
      localStorage.setItem('auth_token_expired', expired.toString());

      return data.jwt_token;
    } else {
      if (localStorage.getItem('is_user')) {
        localStorage.removeItem('is_user');
      }

      throw {
        statusCode: 400,
        error: data,
        message: data.error?.message?.replace('GraphQL error: ', ''),
      };
    }
  }

  public async getToken() {
    let token = localStorage.getItem('auth_token');

    let expired: any = localStorage.getItem('auth_token_expired');
    expired = expired ? moment(expired).utc() : moment().utc();

    if (moment().utc().isAfter(expired)) {
      token = await this.refreshToken();
    }

    return token;
  }

  public async query(
    name: string,
    { query, variables = {}, authHeader = true }: QueryInput
  ) {
    const graphQLClient = await this.getGraphqlClient(authHeader);
    try {
      const data = await graphQLClient.request(query, variables);
      return data[name];
    } catch (error) {
      console.log('[GQL] Error', error);

      try {
        const resError = JSON.parse(JSON.stringify(error, undefined, 2));
        return { error: resError.response };
      } catch (error) {
        return { error };
      }
    }
  }

  public async mutate(
    name: string,
    { mutation, variables = {}, authHeader = true }: MutateInput
  ) {
    return this.query(name, { query: mutation, variables, authHeader });
  }
}

export const ontopoIo = new HttpService('https://dbio4.ontopo.work/v1/graphql');
export const ontopoApi = new HttpService(
  'https://ontopo-api-v1-d2sq6jc35a-uc.a.run.app/graphql'
);

interface QueryInput {
  query: string;
  variables?: any;
  authHeader?: boolean;
}

interface MutateInput {
  mutation: string;
  variables?: any;
  authHeader?: boolean;
}

import { ontopoIo, gql } from 'src/boot/graphql-http-client';

class Service {
  // Service: Get user profile
  public userProfile = async () => {
    const queryGql = gql`
      query UserProfile {
        userProfile {
          user
        }
      }
    `;

    const data = await ontopoIo.query('userProfile', {
      query: queryGql,
      variables: {},
    });

    if (data.user) {
      return data.user;
    } else {
      throw {
        statusCode: 400,
        error: JSON.stringify(data),
        message: data.message?.replace('GraphQL error: ', ''),
      };
    }
  };
}

export default new Service();

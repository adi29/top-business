import { RouteRecordRaw } from 'vue-router';

const routes: RouteRecordRaw[] = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      {
        path: '',
        name: 'HomePage',
        component: () => import('pages/Index.vue'),
      },
      {
        path: 'venues',
        name: 'Venues',
        component: () => import('pages/Venues.vue'),
      },
    ],
  },
  {
    path: '/:cmsId',
    component: () => import('layouts/MenuLayout.vue'),
    children: [
      {
        path: 'dashboard',
        name: 'dashboard',
        component: () => import('pages/venue/Dashboard.vue'),
      },
      {
        path: 'rsvp',
        name: 'rsvp',
        component: () => import('pages/venue/Rsvp.vue'),
      },
      {
        path: 'giftcard',
        name: 'giftcard',
        component: () => import('pages/venue/Giftcard.vue'),
      },
      {
        path: '',
        name: 'venue',
        component: () => import('pages/venue/Index.vue'),
      },
    ],
  },
  {
    path: '/login',
    component: () => import('layouts/InnerLayout.vue'),
    children: [
      {
        path: '',
        name: 'Login',
        component: () => import('pages/login/Index.vue'),
      },
    ],
  },
  {
    path: '/:cmsId',
    component: () => import('layouts/InnerLayout.vue'),
    children: [
      {
        path: 'giftcard/:giftcardId',
        name: 'GiftcardId',
        component: () => import('pages/venue/GiftcardId.vue'),
      },
    ],
  },
  {
    path: '/test',
    component: () => import('pages/test.vue'),
  },
  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/Error404.vue'),
  },
];

export default routes;

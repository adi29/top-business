import { ActionTree } from 'vuex';
import { StateInterface } from '../index';
import { state } from './state';
import service from './service'
import { i18n } from 'src/boot/i18n';
import { Quasar } from 'quasar';


const actions: ActionTree<state, StateInterface> = {
   async setLanguage({}, payload: string) {
    try {
      const lang = payload ? (payload === 'en' ? 'en-us' : payload) : '';      
      const iso = await import('quasar/lang/' + lang);
      Quasar.lang.set(iso.default);
      Object.assign(i18n, {locale: lang});
    } catch (err) {}
  },

  getMenuList ({commit}) {
    const menuList = service.setManagementMenu('taizu');
    commit('setMenu', menuList)
  },
};

export default actions;

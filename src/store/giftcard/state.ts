export interface state {
  giftcards: Array<any> ;
  giftcardTypes: Array<any> ;
  managers: Array<any> ;
  giftcardId: any ;
}

function state(): state {
  return {
    giftcards: [],
    giftcardTypes: [],
    managers: [],
    giftcardId: null
  }
};

export default state;
